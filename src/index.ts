import axios from "axios";
import cheerio from "cheerio";
import express from "express";

const { MongoClient } = require("mongodb");
const uri = "mongodb://localhost:27017/scrapping";
const client = new MongoClient(uri, {
  useNewUrlParser: false,
  useUnifiedTopology: false,
});
let details: any;

const app = express();
app.listen(3000, () => {
  console.log("Example app listening on port 3000!");
});



const url = "https://adconnector.com/"; // URL we're scraping
const AxiosInstance = axios.create(); // Create a new Axios Instance


AxiosInstance.get(url)
  .then( // Once we have data returned ...
    async response => {
      const html = response.data; // Get the HTML from the HTTP request
      const $ = cheerio.load(html); // Load the HTML string into cheerio
      $('footer').each((i, el) => {
        $(el).find('a').each(async (k, elem) => {
          if ($(elem).text() !== "") {
            let url: any = $(elem).attr('href')
            await AxiosInstance.get(url)
              .then( // Once we have data returned ...
                async newResponse => {
                  let headings: any = [];
                  const html = newResponse.data; // Get the HTML from the HTTP request
                  const $ = cheerio.load(html); // Load the HTML string into cheerio
                  const title = $('title');
                  $("h1, h2, h3, h4, h5, h6").map((_, element) => {
                    headings.push({
                      tagName: $(element).get(0).name,
                      text: $(element).text()
                    })
                  });
                  const description = $('meta[name="description"]').attr('content');
                  details = {
                    mainLink: $(elem).attr('href'),
                    title: title.text(),
                    headings,
                    description,
                  }
                  await storeScrapping(details);
                }
              ).catch(console.error); // Error handling
          }
        });
      });
    }
  ).catch(console.error); // Error handling

function storeScrapping(details: any) {
  client.connect(uri, (err: any, db: any) => {
    if (err) throw err;
    console.log("Connected successfully to server");
    db.collection("details")
      .insertOne(details)
      .then((result: any) => {
        console.log("data stored");
        return result;
      })
      .catch((error: any) => console.error(error));
  });
}


app.get('/scrap', (req, res) => {
  return client.connect(uri, async (err: any, db: any) => {
    if (err) throw err;
    console.log("Connected successfully to server");
    db.collection('details').find().toArray((err: any, results: any) => {
      if (err) throw err;
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      res.status(200).send(results);
    })
  });
});
