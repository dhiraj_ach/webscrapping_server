"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var cheerio_1 = __importDefault(require("cheerio"));
var express_1 = __importDefault(require("express"));
var MongoClient = require("mongodb").MongoClient;
var uri = "mongodb://localhost:27017/scrapping";
var client = new MongoClient(uri, {
    useNewUrlParser: false,
    useUnifiedTopology: false,
});
var details;
var app = express_1.default();
app.listen(3000, function () {
    console.log("Example app listening on port 3000!");
});
var url = "https://adconnector.com/"; // URL we're scraping
var AxiosInstance = axios_1.default.create(); // Create a new Axios Instance
AxiosInstance.get(url)
    .then(// Once we have data returned ...
function (response) { return __awaiter(void 0, void 0, void 0, function () {
    var html, $;
    return __generator(this, function (_a) {
        html = response.data;
        $ = cheerio_1.default.load(html);
        $('footer').each(function (i, el) {
            $(el).find('a').each(function (k, elem) { return __awaiter(void 0, void 0, void 0, function () {
                var url_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!($(elem).text() !== "")) return [3 /*break*/, 2];
                            url_1 = $(elem).attr('href');
                            return [4 /*yield*/, AxiosInstance.get(url_1)
                                    .then(// Once we have data returned ...
                                function (newResponse) { return __awaiter(void 0, void 0, void 0, function () {
                                    var headings, html, $, title, description;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                headings = [];
                                                html = newResponse.data;
                                                $ = cheerio_1.default.load(html);
                                                title = $('title');
                                                $("h1, h2, h3, h4, h5, h6").map(function (_, element) {
                                                    headings.push({
                                                        tagName: $(element).get(0).name,
                                                        text: $(element).text()
                                                    });
                                                });
                                                description = $('meta[name="description"]').attr('content');
                                                details = {
                                                    mainLink: $(elem).attr('href'),
                                                    title: title.text(),
                                                    headings: headings,
                                                    description: description,
                                                };
                                                return [4 /*yield*/, storeScrapping(details)];
                                            case 1:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                }); }).catch(console.error)];
                        case 1:
                            _a.sent(); // Error handling
                            _a.label = 2;
                        case 2: return [2 /*return*/];
                    }
                });
            }); });
        });
        return [2 /*return*/];
    });
}); }).catch(console.error); // Error handling
function storeScrapping(details) {
    client.connect(uri, function (err, db) {
        if (err)
            throw err;
        console.log("Connected successfully to server");
        db.collection("details")
            .insertOne(details)
            .then(function (result) {
            console.log("data stored");
            return result;
        })
            .catch(function (error) { return console.error(error); });
    });
}
app.get('/scrap', function (req, res) {
    return client.connect(uri, function (err, db) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (err)
                throw err;
            console.log("Connected successfully to server");
            db.collection('details').find().toArray(function (err, results) {
                if (err)
                    throw err;
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
                res.status(200).send(results);
            });
            return [2 /*return*/];
        });
    }); });
});
