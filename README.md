## Installation

Assuming [NodeJS](https://nodejs.org) is available in system.
If not than you can [download](https://nodejs.org/en/download/) from here.

### `npm install`
To install the dependencies.

## Available Scripts

In the project directory, you can run:

### `npm run serve`

